resource "aws_subnet" "private_subnet_1a" {
  cidr_block = "10.0.32.0/20"
  vpc_id = aws_vpc.cluster_vpc.id

  availability_zone = format("%sa", var.aws_region)

  tags = {
    Name = format("%s-private-1a", var.cluster_name),
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  }
}

resource "aws_subnet" "private_subnet_1c" {
  cidr_block = "10.0.48.0/20"
  vpc_id = aws_vpc.cluster_vpc.id

  availability_zone = format("%sc", var.aws_region)

  tags = {
    Name = format("%s-private-1c", var.cluster_name),
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  }
}

resource "aws_route_table_association" "private_1a" {
  route_table_id = aws_route_table.nat.id
  subnet_id = aws_subnet.private_subnet_1a.id
}

resource "aws_route_table_association" "private_1c" {
  route_table_id = aws_route_table.nat.id
  subnet_id = aws_subnet.private_subnet_1c.id
}