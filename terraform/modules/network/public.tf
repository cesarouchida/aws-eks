resource "aws_subnet" "public_subnet_1a" {
  cidr_block = "10.0.0.0/20"
  vpc_id = aws_vpc.cluster_vpc.id
  map_public_ip_on_launch = true
  availability_zone = format("%sa", var.aws_region)

  tags = {
    Name = format("%s-public-1a", var.cluster_name),
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  }
}

resource "aws_subnet" "public_subnet_1c" {
  cidr_block = "10.0.16.0/20"
  vpc_id = aws_vpc.cluster_vpc.id
  map_public_ip_on_launch = true
  availability_zone = format("%sa", var.aws_region)

  tags = {
    Name = format("%s-public-1c", var.cluster_name),
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  }
}

resource "aws_route_table_association" "public_1a" {
  route_table_id = aws_route_table.igw_route_table.id
  subnet_id = aws_subnet.public_subnet_1a.id
}

resource "aws_route_table_association" "public_1c" {
  route_table_id = aws_route_table.igw_route_table.id
  subnet_id = aws_subnet.public_subnet_1c.id
}