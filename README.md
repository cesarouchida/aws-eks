# aws-eks

Projeto irá abordar a utilização da AWS EKS com o provisionamento com terraform

# Pré-requisitos

### AWS Credentials

Inserir as credenciais da AWS com AssumeRoles necessárias para o criação e alteração do projeto

````
[default]
aws_access_key_id = xxxx
aws_secret_access_key = xxxx
````

### Estruturas 

Breve resumo da criação do terraform
```
├── terraform
│   ├── modules
│   │   ├── master
│   │   │   ├── iam.tf
│   │   │   ├── master.tf
│   │   │   ├── output.tf
│   │   │   ├── security.tf
│   │   │   └── variables.tf
│   │   └── network
│   │       ├── internet.tf
│   │       ├── nat-gateway.tf
│   │       ├── output.tf
│   │       ├── private.tf
│   │       ├── public.tf
│   │       ├── variables.tf
│   │       └── vpc.tf
│   ├── modules.tf
│   ├── output.tf
│   ├── provider.tf
│   └── variables.tf
└── docker-compose.yml
```



- __terraform__

Dentro da raiz da pasta do terraform há todos os arquivos que são usados globalmente para outros modulos.

- __modules__

Contém subdivisões dos terraforms  

